#!/bin/ash
set -euo pipefail

apk add sudo alpine-sdk

adduser -g '' -s /bin/sh -G abuild -D tezos
mkdir -pv /var/cache/distfiles
chgrp abuild /var/cache/distfiles
chmod g+w /var/cache/distfiles

echo 'tezos ALL=(ALL:ALL) NOPASSWD:ALL' > /etc/sudoers.d/tezos
chmod 440 /etc/sudoers.d/tezos

echo "${NOMADIC_RSA_PUBLIC_KEY}"  > /etc/apk/keys/nomadic.rsa.pub

su tezos -c "${CI_PROJECT_DIR}/build.sh"
