# ⚠️ Archived project!

Since https://gitlab.com/tezos/opam-repository/-/merge_requests/421 + https://gitlab.com/tezos/tezos/-/merge_requests/8838
and usage of Alpine `3.17` in the CI docker image these custom packages are no longer required.

Closed by https://gitlab.com/tezos/opam-repository/-/merge_requests/419 + https://gitlab.com/tezos/tezos/-/merge_requests/8741.

# `hidapi` and `hidapi-dev` Alpine packages

Build and push a custom Alpine package `hidapi` and `hidapi-dev` with
shared AND static libraries for https://gitlab.com/tezos/tezos.

Used in Tezos pipelines CI docker image: https://gitlab.com/tezos/opam-repository

Changes got merged to `master` (2022-06-13):
* https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/35251
* https://gitlab.alpinelinux.org/alpine/aports/-/commit/71025c08db4d8df53ee4c4fb5a8a3864720aa49b

Released in Alpine `3.17` (2022-11-22)
https://gitlab.alpinelinux.org/alpine/aports/-/blob/3.17-stable/community/hidapi/APKBUILD

## Installation

* Run Alpine `3.15` or `3.16`

* Add Nomadic's public RSA key for `apk` inside your Alpine image:

  ```sh
  cat <<EOF > /etc/apk/keys/nomadic.rsa.pub
  -----BEGIN PUBLIC KEY-----
  MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA0biIZLo1zfCqiT7HvFlF
  5xQhgI9jtCPC11rUpPtsSOMSqq1TEE5Xxga470R7g6B5Td6MLxG2XILaE5bxUN1s
  lVh4Qay92eacSlYz9btMIg1YdPbZmMAfuL/gHT9GVzIKqizsnx8QfX9AeDai6cGJ
  tJdA0GDcvZXVOLXpPRIt3KkOn+H6PmEqA/PnEPduGagArSC3C1Kpyb0uqE8Vnxs7
  LhkKZCyrYd5AY9XlBeIz9jPYMQhVrKERJ6XFSF0n9Gq91ZVvkdBDhFvkSCDtsBG8
  859Tr+4/wcT0zYespYeC4lzCBPJaIhXERE0fjt9p1Kl1RLT/XpMLC8s7VcGlT7UQ
  hQIDAQAB
  -----END PUBLIC KEY-----
  EOF
  ```

* Download taraball and its checksum from packages: https://gitlab.com/nomadic-labs/hidapi-apk/-/packages

* Verify and extract:

  ```sh
  sha512sum -c hidapi-<version>.<architecture>.tar.sha512
  tar -xvf hidapi-<version>.<architecture>.tar
  ```

* Install packages:

  ```sh
  apk add ./hidapi-<version>.apk ./hidapi-dev-<version>.apk
  ```

## Resources

* [`hidapi` GitHub issue](https://github.com/libusb/hidapi/issues/424)

* Alpine documentation:
  * https://wiki.alpinelinux.org/wiki/Creating_an_Alpine_package
  * https://wiki.alpinelinux.org/wiki/APKBUILD_Reference


* Alpine `aports` merge requests by @abate:
  * `libusb` https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/23613
  * `hidapi` https://gitlab.alpinelinux.org/alpine/aports/-/merge_requests/23615

* Alpine packages:
  * `libusb` in `main`: https://gitlab.alpinelinux.org/alpine/aports/-/tree/master/main/libusb
  * `hidapi` in `community`: https://gitlab.alpinelinux.org/alpine/aports/-/tree/master/community/hidapi
