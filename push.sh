#!/bin/sh
set -eu

### Create a GitLab package with the created APK files

gitlab_package_name="${CI_PKG_NAME}-${CI_PKG_VER}-r${CI_PKG_REL}"
gitlab_package_version="${CI_PKG_VER}"

# https://docs.gitlab.com/ee/user/packages/generic_packages/index.html#download-package-file
# :gitlab_api_url/projects/:id/packages/generic/:package_name/:package_version/:file_name
gitlab_package_url="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${gitlab_package_name}/${gitlab_package_version}"

gitlab_upload() {
  local_path="${1}"
  remote_file="${2}"
  echo "Upload to ${gitlab_package_url}/${remote_file}"

  i=0
  max_attempts=10

  # Retry because gitlab.com is flaky sometimes, curl upload fails with http status code 524 (timeout)
  while [ "${i}" != "${max_attempts}" ]
  do
    i=$((i + 1))
    http_code=$(curl -fsSL -o /dev/null -w "%{http_code}" \
                     -H "JOB-TOKEN: ${CI_JOB_TOKEN}" \
                     -T "${local_path}" \
                     "${gitlab_package_url}/${remote_file}")

    # Success
    [ "${http_code}" = '201' ] && return
    # Failure
    echo "Error: HTTP response code ${http_code}, expected 201"
    # Do not backoff after last attempt
    [ "${i}" = "${max_attempts}" ] && break
    # Backoff
    echo "Retry (${i}) in one minute..."
    sleep 60s
  done

  echo "Error: maximum attempts exhausted (${max_attempts})"
  exit 1
}

tar_file="${CI_PKG_NAME}-${CI_PKG_VER}-r${CI_PKG_REL}.$(arch).tar"

cd "/home/tezos/packages/${CI_PROJECT_NAMESPACE}/$(arch)"

tar -cvf "${tar_file}" *

sha512sum "${tar_file}" > "${tar_file}.sha512"

gitlab_upload "${tar_file}" "${tar_file}"
gitlab_upload "${tar_file}.sha512" "${tar_file}.sha512"
