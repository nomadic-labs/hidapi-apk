#!/bin/ash
set -euo pipefail

private_key='nomadic.rsa'
public_key="${private_key}.pub"

# Verify we are using a non-root user in group 'abuild'
whoami
id
pwd

mkdir -pv "${HOME}/.abuild"
echo "PACKAGER_PRIVKEY=\"${HOME}/.abuild/${private_key}\"" > "${HOME}/.abuild/abuild.conf"
touch "${private_key}"
chmod 600 "${private_key}"
echo "${NOMADIC_RSA_PRIVATE_KEY}" > "${HOME}/.abuild/${private_key}"
echo "${NOMADIC_RSA_PUBLIC_KEY}"  > "${HOME}/.abuild/${public_key}"

abuild -r
