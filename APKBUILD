# Fork from https://git.alpinelinux.org/aports/tree/community/hidapi/APKBUILD?h=3.16-stable
# https://github.com/libusb/hidapi/blob/hidapi-0.11.2/.github/workflows/builds.yml#L56
pkgname="$CI_PKG_NAME"
pkgver="$CI_PKG_VER"
pkgrel="$CI_PKG_REL"
pkgdesc="Simple library for communicating with USB and Bluetooth HID devices"
url="https://github.com/libusb/hidapi"
arch="all"
license="GPL-3.0-only OR BSD-3-Clause"
makedepends="
  cmake
  eudev-dev
  libusb-dev
  linux-headers
  "
subpackages="$pkgname-dev"
source="https://github.com/libusb/hidapi/archive/hidapi-$pkgver/hidapi-$pkgver.tar.gz"
options="!check"  # No tests
builddir="$srcdir/hidapi-hidapi-$pkgver"

# Install static version first, and then shared, so by default CMake users would use
# a shared version of the library, to be consistent with pkgconfig users in general

build() {
  # Static libraries
  cmake -B build/static -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_INSTALL_LIBDIR=lib -DBUILD_SHARED_LIBS=FALSE
  cmake --build build/static
  # Shared libraries
  cmake -B build/shared -DCMAKE_INSTALL_PREFIX=/usr -DCMAKE_INSTALL_LIBDIR=lib -DBUILD_SHARED_LIBS=TRUE
  cmake --build build/shared
}

package() {
  # Static libraries
  DESTDIR="$pkgdir" cmake --install build/static
  # Shared libraries
  DESTDIR="$pkgdir" cmake --install build/shared
}

sha512sums="
c4d04bf570aa98dd88d7ce08ef1abb0675d500c9aa2c22f0437fa30b700a94446779f77e1170267926d5f6f0d9cdb2bb81ad1fe20d158c18587fddbca59e9517  hidapi-0.11.2.tar.gz
"
